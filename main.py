import time

from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager

from helpers import *
from zad1 import get_all_emails, compare_mails
from zad2 import fill_form
from zad3 import get_images
from zad4 import plot, calculate_knn_and_print_metrics


BASE_URL = "https://www.kis.p.lodz.pl/staff.170.html"
URL_ZAD_2 = "http://127.0.0.1:5000/"

url_info1 = UrlInfo(
    "https://pl.depositphotos.com/stock-photos/pi%C5%82ka-no%C5%BCna.html",
    "piłka nożna",
    "ball/"
)

url_info2 = UrlInfo(
    "https://pl.depositphotos.com/stock-photos/kotek.html",
    "koty",
    "cats/"
)

url_info3 = UrlInfo(
    "https://pl.depositphotos.com/stock-photos/pustynia.html",
    "pustynia",
    "dessert/"
)

url_info4 = UrlInfo(
    "https://pl.depositphotos.com/stock-photos/cz%C5%82owiek.html",
    "czlowiek",
    "human/"
)

url_info5 = UrlInfo(
    "https://pl.depositphotos.com/stock-photos/mi%C5%9B.html",
    "misie",
    "bear/"
)

MAX_IMAGES_PER_CATEGORY = 50


COLOR_MAPPER = {
    0: "blue",
    1: "green",
    2: "red"
}

DATA = [url_info1, url_info2, url_info3, url_info4, url_info5]


def init_driver():
    driver_selenium = webdriver.Chrome(ChromeDriverManager().install())
    options = webdriver.ChromeOptions()
    options.add_argument('headless')
    return driver_selenium


def save_images_in(data_to_fill):
    for url_info in data_to_fill:
        time.sleep(0.1)
        found_images = get_images(url_info.base_url, url_info.folder_name, MAX_IMAGES_PER_CATEGORY)
        converted_images = convert_images_to_opencv(found_images)
        url_info.add_images(converted_images)


if __name__ == "__main__":
    driver = init_driver()

    # zad 1
    mails_collected_by_old_way_and_selenium = get_all_emails(driver, BASE_URL)
    compare_mails(mails_collected_by_old_way_and_selenium[0],
                  mails_collected_by_old_way_and_selenium[1])

    # zad 2
    fill_form(driver, URL_ZAD_2)

    # zad 3
    save_images_in(DATA)

    # zad 4
    plot(DATA)
    calculate_knn_and_print_metrics(DATA)
