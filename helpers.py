from cv2 import *
import contextlib
import re
import tempfile
from urllib.error import ContentTooShortError
from urllib.request import urlopen, Request, _url_tempfiles
from urllib.parse import splittype
import os


class UrlInfo:
    images = []

    def __init__(self, base_url, domain_name, folder_name):
        self.base_url = base_url
        self.domain_name = domain_name
        self.folder_name = folder_name

    def add_images(self, images_filenames):
        self.images = images_filenames

    def __str__(self):
        return f"Dla {self.base_url} znaleziono:\n{self.images}\n zapisanych w {self.folder_name}"

    def __repr__(self):
        return f"Dla {self.base_url} znaleziono:\n{len(self.images)}\n zapisanych w {self.folder_name}"


# Funkcja pomocnicza konwertuje obrazek za pomocą funkcji imread
def convert_images_to_opencv(images):
    opencv_images = []
    for image in images:
        converted = cv2.imread(image)
        opencv_images.append(converted)
    return opencv_images


# Funkcja walidująca poprawność rozszerzenia dla zdjęcia
def is_image(out_path: str):
    out_path = out_path.lower()
    image_type_regex = r"""^.*\.(jpg|JPG|PNG|png|jpeg|JPEG|tiff|TIFF|bmp|BMP)$"""
    return re.match(image_type_regex, out_path)


# Rozszerzenie metody urlretrieve o dodatkowe nagłówki, pierwotna metoda, nie posiada ich, co czasem powodowało błąd podczas pobierania obrazków
def urlretrieve_with_headers(url, filename=None):
    hdr = {
        'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.64 Safari/537.11',
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
        'Accept-Charset': 'ISO-8859-1,utf-8;q=0.7,*;q=0.3',
        'Accept-Encoding': 'none',
        'Accept-Language': 'en-US,en;q=0.8',
        'Connection': 'keep-alive'}
    req = Request(url, headers=hdr)
    url_type, path = splittype(url)

    with contextlib.closing(urlopen(req)) as fp:
        headers = fp.info()

        if url_type == "file" and not filename:
            return os.path.normpath(path), headers

        if filename:
            tfp = open(filename, 'wb')
        else:
            tfp = tempfile.NamedTemporaryFile(delete=False)
            filename = tfp.name
            _url_tempfiles.append(filename)

        with tfp:
            result = filename, headers
            bs = 1024 * 8
            size = -1
            read = 0
            blocknum = 0
            if "content-length" in headers:
                size = int(headers["Content-Length"])

            while True:
                block = fp.read(bs)
                if not block:
                    break
                read += len(block)
                tfp.write(block)
                blocknum += 1

    if size >= 0 and read < size:
        raise ContentTooShortError(
            "retrieval incomplete: got only %i out of %i bytes"
            % (read, size), result)

    return result


# Funkcja tworząca folder
def create_folder(folder_name):
    if not os.path.exists(folder_name):
        os.makedirs(folder_name)
