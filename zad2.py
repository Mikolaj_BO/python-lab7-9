from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait


# Wypełnia formularz oraz wyświetla wynik wypełnionego formularza
# http://127.0.0.1:5000/"
def fill_form(driver, url):
    try:
        driver.get("http://127.0.0.1:5000/")

        button_to_open_form = driver.find_element_by_css_selector("body > input[type=button]")
        button_to_open_form.click()

        wait = WebDriverWait(driver, 10)
        wait.until(EC.presence_of_element_located((By.CSS_SELECTOR, "#form > form > div > button")))

        submit = driver.find_element_by_css_selector("#form > form > div > button")

        # Znalezienie elementów formularza
        email = driver.find_element_by_id("email")
        name = driver.find_element_by_id("name")
        password = driver.find_element_by_id("password")
        password_repeat = driver.find_element_by_id("password-repeat")
        answer = driver.find_element_by_id("answer")

        # Wypełnienie formularza
        email.send_keys("eksploracja@eksploracja.kis.p.lodz.pl")
        name.send_keys("eksploracja")
        password.send_keys("tajnehaslo")
        password_repeat.send_keys("tajnehaslo")
        answer.send_keys("Poprawna odpowiedź!")

        submit.click()

        # Odpowiedź wypełnionego formularza
        wait.until(EC.url_changes(url))
        result = driver.find_element_by_xpath("/html/body")
        print(f"Wynik formularza:\n {result.text}")
    except:
        print("Wystąpił błąd podczas wypełniania formularza, czy strona jest aktywna?\n\n")
