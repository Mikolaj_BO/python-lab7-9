from bs4 import BeautifulSoup
from urllib.parse import urljoin
from helpers import *
import os

from tqdm import tqdm


# Funkcja pobierająca obrazki dla danego URL.
# Parametr out_folder docelowy folder zapisu obrazków
# Parametr max określa ile maksyymalnie zdjęć może pobrać z danej strony
def get_images(url, out_folder, max):
    create_folder(out_folder)
    images_filenames = set()
    hdr = {
        'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.64 Safari/537.11',
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
        'Accept-Charset': 'ISO-8859-1,utf-8;q=0.7,*;q=0.3',
        'Accept-Encoding': 'none',
        'Accept-Language': 'en-US,en;q=0.8',
        'Connection': 'keep-alive'}
    req = Request(url, headers=hdr)

    soup = BeautifulSoup(urlopen(req), features="html.parser")

    print(f"Przetwarzam obrazki dla : {url}")
    images = soup.findAll("img")
    progressBar = tqdm(images[:max])
    for image in progressBar:
        if 'src' in image.attrs:
            image_url = urljoin(url, image['src'])
            filename = image["src"].split("/")[-1]
            outpath = os.path.join(out_folder, filename)

            image_filename = validate_and_save(outpath, image_url)
            if image_filename and len(images_filenames) < max:
                images_filenames.add(image_filename)

    return images_filenames


# Funkcja walidująca poprawność url do zdjęcia, oraz zapisująca obrazek pobrany z url do pliku
def validate_and_save(out_path, image_url):
    out_path = re.sub(r"""[:*?"<>|]""", "", out_path)
    if is_image(out_path):
        try:
            urlretrieve_with_headers(image_url, out_path)
            return out_path
        except:
            print(f'\nWystąpił błąd podczas pobierania obrazka dla: {image_url}\n'
                  f' pomijam...\n')
            return None