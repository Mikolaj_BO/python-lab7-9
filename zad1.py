import re
import time

from tqdm import tqdm
from requests_html import HTMLSession

EMAIL_REGEX_OLD = r"""(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]))\.){3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])"""


# Nowe rozwiązanie z użyciem Selenium
def get_email_new(driver, url):
    driver.get(url)
    all_email = set()

    doc = driver.page_source

    emails = re.findall(r'[\w\.-]+@[\w\.-]+', doc)
    all_email.update(emails)

    return emails


# Szuka maili na stronie - rozwiązanie z pierwszych labolatoriów
def get_emails_old(url):
    emails = set()
    session = HTMLSession()
    r = session.get(url)
    r.html.render()

    for re_match in re.finditer(EMAIL_REGEX_OLD, r.html.raw_html.decode()):
        emails.add(re_match.group())
    return emails


# Pobiera zagnieżdżone linki
def get_nested_links_for(driver, url):
    driver.get(url)
    nested_links = set()
    elems = driver.find_elements_by_xpath("//a[@href]")
    for elem in elems:
        link = elem.get_attribute("href")
        if url in link:
            nested_links.add(link)
    return nested_links


# Pobiera maile dla wszystkich zagnieżdżonych linków za pomocą selenium, oraz starym sposobem
def get_all_emails(driver, url):
    old_emails = set()
    new_emails = set()
    all_links = get_nested_links_for(driver, url)
    print(f"Znaleziono: {all_links}")
    print("Szukam maili")
    progress_Bar = tqdm(all_links)
    for link in progress_Bar:
        time.sleep(0.25)
        old_emails_temp = get_emails_old(link)
        new_emails_temp = get_email_new(driver, link)
        new_emails.update(new_emails_temp)
        old_emails.update(old_emails_temp)

    time.sleep(0.25)
    return old_emails, new_emails


# Wyświetla oraz porównuje pobrane maila metodą selenium oraz starym sposobem
def compare_mails(old_way_to_get_mails: set, selenium_mails: set):
    print(f"\nMaile starym sposobem: {old_way_to_get_mails}\n")
    print(f"Maile nowym sposobem: {selenium_mails}\n")

    print(f"Liczba maili dla starego sposobu: {len(old_way_to_get_mails)}")
    print(f"Liczba maile nowym sposobem: {len(selenium_mails)}\n")

    if old_way_to_get_mails != selenium_mails:
        print(f"Nie znalezione maile: {selenium_mails.difference(old_way_to_get_mails)}\n")
